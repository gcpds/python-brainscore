import matplotlib
matplotlib.use('TkAgg', warn=False, force=True)
import matplotlib.pyplot as plt
import matplotlib.animation as animation
#matplotlib.rcParams['toolbar'] = 'None'

import numpy as np

import logging
import threading
from queue import Queue

from brainscore import BrainScore
from brainscore import SAMPLES_TO_PROCESS, FEATURES, PACK_LENGTH, CHANNELS, STIMULUS_FRQUENCY

from frames import MplFrame


SPECGRAM_SIZE = SAMPLES_TO_PROCESS


########################################################################
class BrainsCorePlot(BrainScore):
    """"""

    #----------------------------------------------------------------------
    def __init__(self):
        """Constructor"""

        super().__init__()

        self.fig = plt.figure('BrainScore')

        self.data = Queue()
        self.specgram = Queue(SPECGRAM_SIZE)

        plt.style.use('dark_background')
        plt.rcParams['axes.facecolor'] = 'k'
        #plt.subplots_adjust(wspace=0, hspace=0,
                            #left=0.05, right=0.99, bottom=0.05, top=0.99)
        self.fig.set_facecolor('k')

        self.eeg = MplFrame(subplot=221)
        #self.eeg.x = [0]
        self.eeg.lines = CHANNELS.split()
        self.eeg.ax.set_ylim(0, 9)
        #self.eeg.ax.set_xlim(0, samples / openbci.sample_rate)
        self.eeg.ax.set_xlabel("Time [s]")
        self.eeg.ax.set_ylabel("Channels")
        self.eeg.ax.grid(True, color=(0.25, 0.25, 0.25))

        self.frequency = MplFrame(subplot=222)
        #self.frequency.x = np.linspace(0, openbci.sample_rate / 2, samples // 2)
        self.frequency.lines = 1
        self.frequency.ax.set_ylim(0, 1.2)
        #self.frequency.ax.set_xlim(0, openbci.sample_rate / 2)

        self.frequency.ax.set_xlabel("Frequency [Hz]")
        self.frequency.ax.set_ylabel("Amplitude")
        self.frequency.ax.grid(True, color=(0.25, 0.25, 0.25))
        self.frequency.ax.vlines(STIMULUS_FRQUENCY, 0, 1.2, color='C1', linestyle='--', linewidth=1)

        for frq in STIMULUS_FRQUENCY:
            self.frequency.ax.annotate(f"{frq} Hz", (frq + 1, 1.15), color='C1', fontsize=7)

        self.bars_e = MplFrame(subplot=247)
        self.bars_e.bars = 4
        self.bars_e.ax.set_xticks(range(4))
        self.bars_e.ax.set_xticklabels(FEATURES.split()[0:8:2])
        self.bars_e.ax.set_xlim(-1, 4)
        self.bars_e.ax.set_ylim(0, 1)
        self.bars_e.ax.set_ylabel("Energy")

        self.bars_e.ax.grid(True, color=(0.25, 0.25, 0.25), zorder=0)

        for i, bar in enumerate(self.bars_e.bars):
            bar.set_height(0)
            bar.set_x(i - bar.get_width() / 2)

        self.bars_h = MplFrame(subplot=248)
        self.bars_h.bars = 4
        self.bars_h.ax.set_xticks(range(4))
        self.bars_h.ax.set_xticklabels(FEATURES.split()[1:8:2])
        self.bars_h.ax.set_xlim(-1, 4)
        self.bars_h.ax.set_ylim(0, 1)
        self.bars_h.ax.set_ylabel("Entropy")

        self.bars_h.ax.grid(True, color=(0.25, 0.25, 0.25), zorder=0)

        for i, bar in enumerate(self.bars_h.bars):
            bar.set_height(0)
            bar.set_x(i - bar.get_width() / 2)

        #Specgram
        self.spec = plt.subplot(223)
        self.spec.set_xlabel("Time [s]")
        self.spec.set_ylabel("Frequency [Hz]")

    #----------------------------------------------------------------------
    def animate(self):
        """"""
        self.UPDATING = False
        _ = animation.FuncAnimation(self.fig, self.update, interval=10)
        plt.show()

    #----------------------------------------------------------------------
    def stream(self, *args):
        """"""
        self.data.put(args)

    #----------------------------------------------------------------------
    def update(self, *args):
        """"""
        if self.UPDATING:
            return

        self.UPDATING = True

        data_buffer = self.data.qsize()

        if not data_buffer:
            self.UPDATING = False
            return

        if data_buffer > 1:
            logging.warning(f'Plot buffer: {data_buffer}')

        [self.data.get() for i in range(self.data.qsize() - 1)]
        data, w, yw, fi, features, stimulus = self.data.get()

        #Plot EEG
        for i, d in enumerate(data):
            self.eeg.lines[i].set_ydata(2 * (d - d.mean()) + i + 1)
            self.eeg.lines[i].set_xdata(np.linspace(0, d.shape[0] / self.openbci.sample_rate, d.shape[0]))
            self.eeg.ax.set_xlim(0, d.shape[0] / self.openbci.sample_rate)

        #Plot spectrum
        cut = self.find_nearest(45, w)
        yw = yw[: cut]
        w = w[: cut]
        #cut = -1
        self.frequency.lines[0].set_ydata(yw)
        self.frequency.lines[0].set_xdata(w)
        self.frequency.ax.set_xlim(0, w[-1])

        #Show frequency
        if hasattr(self.frequency, 'text'):
            self.frequency.text.remove()
        yw_max = np.max(yw)
        w_max = w[yw.argmax()]
        self.frequency.text = self.frequency.ax.annotate(
            f'F{fi}:{w_max:.3} Hz',
            xy=(w_max, yw_max),
            xytext=(w_max + 0.1, yw_max + 0.05),
            arrowprops=dict(arrowstyle="->"),
            color='C3')

        #Show stimulus detected
        if stimulus:
            if hasattr(self.frequency, 'last_stimulus'):
                self.frequency.last_stimulus.remove()
            self.frequency.last_stimulus = self.frequency.ax.vlines(stimulus, 0, 1.2, color='C3', linestyle='-')

        #Show features bars
        for label, bar in zip(self.bars_e.ax.get_xticklabels(), self.bars_e.bars):
            bar.set_height(features[label.get_text()])
        self.bars_e.ax.set_ylim(0, max(max(features.values()) + 0.1, 0.5))

        for label, bar in zip(self.bars_h.ax.get_xticklabels(), self.bars_h.bars):
            bar.set_height(features[label.get_text()])
        self.bars_h.ax.set_ylim(0, max(max(features.values()) + 0.1, 0.5))

        #Specgram
        if w.shape[0] > 75:
            if self.specgram.full():
                self.specgram.get()
            self.specgram.put(yw)
            self.spec.pcolormesh(np.linspace(0, SPECGRAM_SIZE * PACK_LENGTH / 1000, self.specgram.qsize()), w, np.array(self.specgram.queue).T)
            self.spec.set_xlim(0, SPECGRAM_SIZE * PACK_LENGTH / 1000)

        self.UPDATING = False


if __name__ == '__main__':
    plot = BrainsCorePlot()
    plot.process()
    plot.animate()


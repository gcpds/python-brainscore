import matplotlib
matplotlib.use('TkAgg', warn=False, force=True)
import matplotlib.pyplot as plt
import matplotlib.animation as animation
matplotlib.rcParams['toolbar'] = 'None'


from frames import MplFrame


stimulus_freq = [8, 15, 20, 25]
colors = ['b', 'y', 'g', 'r']
SIZE = 7


########################################################################
class BrainscoreStimulus:
    """"""

    #----------------------------------------------------------------------
    def __init__(self):
        """Constructor"""

        #self.fig = plt.figure("Stimulus", figsize=(16, 9), facecolor='w', dpi=100)
        self.fig = plt.figure("Stimulus")

        plt.style.use('dark_background')
        self.fig.set_facecolor('k')
        plt.subplots_adjust(wspace=0, hspace=0,
                            left=0.01, right=0.99, bottom=0.01, top=0.99)

        plt.tick_params(top=False, bottom=False, left=False, right=False, labelleft=False, labelbottom=True)

        #Top
        self.ax1 = plt.subplot(*(SIZE, SIZE, ((SIZE // 2) + 1)))
        self.ax1.set_xticks([])
        self.ax1.set_yticks([])

        #Right
        self.ax2 = plt.subplot(*(SIZE, SIZE, SIZE * ((SIZE // 2) + 1)))
        self.ax2.set_xticks([])
        self.ax2.set_yticks([])

        #Bottom
        self.ax3 = plt.subplot(*(SIZE, SIZE, SIZE * SIZE - (SIZE // 2)))
        self.ax3.set_xticks([])
        self.ax3.set_yticks([])

        #Left
        self.ax4 = plt.subplot(*(SIZE, SIZE, SIZE * (SIZE // 2) + 1))
        self.ax4.set_xticks([])
        self.ax4.set_yticks([])

    #----------------------------------------------------------------------
    def update(self, frame, ax, c):
        """"""
        if sum(ax.get_facecolor()) > 1:
            ax.set_facecolor('k')
        else:
            ax.set_facecolor(c)

    #----------------------------------------------------------------------
    def start(self):
        """"""
        _1 = animation.FuncAnimation(self.fig, self.update, fargs=(self.ax1, colors[0]), interval=1000 / (stimulus_freq[0] * 2))
        _2 = animation.FuncAnimation(self.fig, self.update, fargs=(self.ax2, colors[1]), interval=1000 / (stimulus_freq[1] * 2))
        _3 = animation.FuncAnimation(self.fig, self.update, fargs=(self.ax3, colors[2]), interval=1000 / (stimulus_freq[2] * 2))
        _4 = animation.FuncAnimation(self.fig, self.update, fargs=(self.ax4, colors[3]), interval=1000 / (stimulus_freq[3] * 2))

        plt.show()


if __name__ == '__main__':
    stimulus = BrainscoreStimulus()
    stimulus.start()


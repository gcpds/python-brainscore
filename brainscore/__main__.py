import subprocess
import os


brainscore = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'plot.py')
stimulus = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'stimulus.py')


brainscore_subprocess = subprocess.Popen(f"python {brainscore}", shell=True)
stimulus_subprocess = subprocess.Popen(f"python {stimulus}", shell=True)


input("Any key to exit.\n")

brainscore_subprocess.kill()
stimulus_subprocess.kill()

import os
import sys
import logging
import threading
from multiprocessing import Pool
from queue import Queue, Empty
from datetime import datetime


logging.basicConfig(stream=sys.stdout, level=logging.WARN)

from scipy import stats
import numpy as np

from openbci.database import load_sample_8ch_bin
from openbci.acquisition import CytonRFDuino as Cyton
from openbci.preprocess import eeg_features, eeg_filters


from osc_stream import OSCStream


PACK_LENGTH = 175
SAMPLES_TO_PROCESS = 10
STIMULUS_FRQUENCY = [8, 15, 20, 25]
NOW = datetime.now().timestamp()
DIR_LOGS = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'logs', str(NOW))
BANDS = ['delta', 'theta', 'alpha', 'beta']
FEATURES = 'E(delta) H(delta) E(theta) H(theta) E(alpha) H(alpha) E(beta) H(beta) F0 F1 F2 F3'
CHANNELS = 'Fp1 Fp2 C3 Cz C4 O1 Oz O2'


OSC_IP = "192.168.1.200"
OSC_ADDR = 2020


########################################################################
class BrainScore:
    """"""

    # ----------------------------------------------------------------------
    def __init__(self):
        """Constructor"""

        #self.openbci = Cyton(file=load_sample_8ch_bin(2))
        self.openbci = Cyton()

        self.openbci.calculate_sample_rate()
        logging.info(f"Sampe rate: {self.openbci.sample_rate}")

        self.openbci.reset_buffers()
        self.openbci.reset_input_buffer()

        self.openbci.start_collect()
        self.samples_ = self.openbci.pack_data(milliseconds=PACK_LENGTH)

        self.process_data = Queue(SAMPLES_TO_PROCESS)
        self.processed_data = Queue()
        self.HIST_FRQ = Queue(maxsize=10)

        self.oscstream = OSCStream(OSC_IP, OSC_ADDR)
        self.create_log()

        for i in range(10):
            self.HIST_FRQ.put(0)

    # ----------------------------------------------------------------------
    def process_in_thread(self):
        """"""
        threading.Timer(SAMPLES_TO_PROCESS / 1000, self.process).start()

    # ----------------------------------------------------------------------
    def find_nearest(self, value, list_):
        """"""
        array = np.asarray(list_)
        idx = (np.abs(array - value)).argmin()
        return idx

    # ----------------------------------------------------------------------
    def get_features(self, data):
        """"""
        features = {}

        for band in BANDS:
            data_band = getattr(eeg_filters, band)(data)

            features[f'E({band})'] = np.mean(eeg_features.energy(data_band)) / np.mean(eeg_features.energy(data))
            features[f'H({band})'] = np.mean(eeg_features.entropy(data_band))

        return features

    # ----------------------------------------------------------------------
    def get_stumilus_frequency(self, w, yw):
        """"""

        # Get stimulus frequency
        #Yf_max = np.max(yw)
        w_max = w[yw.argmax()]

        fi = self.find_nearest(w_max, STIMULUS_FRQUENCY)

        F = np.zeros_like(STIMULUS_FRQUENCY)

        if self.HIST_FRQ.full():
            self.HIST_FRQ.get()
        self.HIST_FRQ.put(fi)

        mode_ = stats.mode(self.HIST_FRQ.queue)

        if mode_.count[0] >= 5 and mode_.mode[0] > 0:
            F[mode_.mode[0]] = 1

        return F

    # ----------------------------------------------------------------------
    def create_log(self):
        """"""
        os.mkdir(DIR_LOGS)
        with open(os.path.join(DIR_LOGS, f"about.log"), 'a') as file:
            file.write(f"Date: {datetime.fromtimestamp(NOW)}\n")
            file.write(f"Sample rate: {self.openbci.sample_rate}  Hz\n")
            file.write(f"Samples by package: {self.samples_}\n")
            file.write(f"Package size: {SAMPLES_TO_PROCESS}\n")
            file.write(f"Package time: {PACK_LENGTH} ms\n")

        with open(os.path.join(DIR_LOGS, f"eeg.log"), 'a') as file:
            file.write(', '.join(CHANNELS.split()) + '\n')

        with open(os.path.join(DIR_LOGS, f"features.log"), 'a') as file:
            file.write(', '.join(FEATURES.split()) + '\n')

    # ----------------------------------------------------------------------
    def add_log(self, eeg, features_):
        """"""
        with open(os.path.join(DIR_LOGS, f"eeg.log"), 'a') as file:
            for l in eeg.T:
                file.write(str(l.tolist()).replace('[', '').replace(']', '') + '\n')

        with open(os.path.join(DIR_LOGS, f"features.log"), 'a') as file:
            file.write(str(list(features_.values())).replace('[', '').replace(']', '') + '\n')

    # ----------------------------------------------------------------------
    def process(self):
        """"""
        threading.Timer((PACK_LENGTH / 1000) / 5, self.process).start()

        if self.openbci.eeg_pack.empty():
            return

        # remove the last packa
        if self.process_data.full():
            self.process_data.get()

        # Update pack
        eeg, aux = self.openbci.eeg_pack.get()
        self.process_data.put(eeg)

        # create a unique array with the pack
        data_raw = self.process_data.queue
        data = np.concatenate(data_raw, axis=1)

        # Filters
        data = eeg_filters.notch60(data)
        data = eeg_filters.band245(data)
        #cut_spectrum = 45

        # Get features
        features = self.get_features(data)

        # Spectrum
        w, yw = eeg_features.welch(data[5:], fs=self.openbci.sample_rate)
        yw = yw.mean(axis=0) / np.max(yw.mean(axis=0))  # normalization

        # Update features with stimulus frequency
        F = self.get_stumilus_frequency(w, yw)
        stimulus = None
        for i, f in enumerate(F):
            features[f'F{i+1}'] = f
            if f:
                stimulus = STIMULUS_FRQUENCY[i]

        # send stream
        try:
            self.oscstream.send(**features)
        except:
            logging.warn(f'No OSC server at {OSC_IP}:{OSC_ADDR}')
            self.oscstream.close()
            self.oscstream = OSCStream(OSC_IP, OSC_ADDR)

        # save_log
        self.add_log(data_raw[-1], features)

        self.stream(data, w, yw, i + 1, features, stimulus)

    # ----------------------------------------------------------------------
    def stream(self, *args, **kwargs):
        """"""


if __name__ == '__main__':
    brainscore = BrainScore()
    brainscore.process()




from osc4py3 import as_eventloop as osc_loop
from osc4py3 import oscbuildparse
import logging


########################################################################
class OSCStream:
    """"""

    #----------------------------------------------------------------------
    def __init__(self, ip, addr):
        """Constructor"""

        # Start the system.
        osc_loop.osc_startup()

        # Make client channels to send packets.
        osc_loop.osc_udp_client(ip, addr, "MaxMSP")

        self.features = ['F1', 'F2', 'F3', 'F4',
                         'E(delta)', 'H(delta)',
                         'E(theta)', 'H(theta)',
                         'E(alpha)', 'H(alpha)',
                         'E(beta)', 'H(beta)']

    #----------------------------------------------------------------------
    def send(self, **kwargs):
        """"""
        message = [format(kwargs[feature], '.20f') for feature in self.features]
        msg = oscbuildparse.OSCMessage("/BrainRhythms", None, message)
        osc_loop.osc_send(msg, "MaxMSP")
        osc_loop.osc_process()
        logging.info(f"Sended through OSC: {message}")

    #----------------------------------------------------------------------
    def close(self):
        """"""
        # Properly close the system.
        osc_loop.osc_terminate()

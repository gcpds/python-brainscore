"""
=========
WSHandler
=========

WebSockets are not standard HTTP connections. The "handshake" is HTTP,
but after the handshake, the protocol is message-based.

This implementation suppose that input message are in JSON format, each input
message will trigger a response message (in JSON format too) with a ``debug``
information.
"""

import json

from tornado.ioloop import IOLoop
from tornado import websocket
from datetime import timedelta

import traceback


CLIENTS = []


########################################################################
class WSHandler(websocket.WebSocketHandler):
    """Websocket handler."""

    NAME = None


    #----------------------------------------------------------------------
    def initialize(self, device_config):
        """Load config values from main app.

        Parameters
        ----------
        device_config: dict
            Dictionary from frame.
        """

        self.device_config = device_config


    #----------------------------------------------------------------------
    def check_origin(self, origin):
        """Prevents a WebSocket from being used by another website that the user is also visiting."""

        return True


    #----------------------------------------------------------------------
    def open(self, *args, **kwargs):
        """Return a message to client for confirm connection."""

        IOLoop.instance().add_timeout(timedelta(seconds=1), self.send_data, {'type': 'LOG','message': 'CONNECTED',})
        self.logger.put('Websocket OK.')
        CLIENTS.append(self)


    #----------------------------------------------------------------------
    def on_close(self):
        """Stop IOLoop and debug connection."""

        # # IOLoop.instance().stop()
        if self in CLIENTS:
            CLIENTS.pop(CLIENTS.index(self))
        self.logger.put("Connection closed.")


    #----------------------------------------------------------------------
    def on_message(self, message):
        """Process input message.

        Parameters
        ----------
        message: dict
            Dictionary with 'command' key as python method.
        """

        data = json.loads(message)

        if not data.get('command', False):
            self.send_data({'type': 'ERROR','message': 'No argument "command" in this request.',}, data)
            return

        command = data.get('command')
        if getattr(self, command, False):  #command is a method
            try:
                self.logger.put("Running command: \"{}\"".format(command))
                getattr(self, command)(**data)  #call method
            except AttributeError as e:
                t = {}
                t['message'] = str(e)
                t['traceback'] = traceback.format_exc()
                self.send_data({'type': 'ERROR', 'message': t,}, data)
        else:
            self.send_data({'type': 'ERROR','message': 'No argument "command" in this request.',}, data)


    #----------------------------------------------------------------------
    def send_data(self, data, request={}):
        """Return data to client.

        Parameters
        ----------
        data: dict
            Dictionary with desired data to return.
        request: dict
            Dictionary with request parameters, like command.
        """

        status_name = 'debug'

        data[status_name] = self.__get_debug__()  #debug data

        data[status_name]['origin'] = self.NAME
        data[status_name]['command'] = request.get('command', None)

        type_ = data.pop('type', None)
        if type_:
            data[status_name]['type'] = type_

        msg = data.pop('message', None)
        if msg:
            data[status_name]['message'] = msg

        if data[status_name]['command']:

            if type_ in ['LOG', 'ERROR']:
                self.logger.put('{}'.format(msg))
            else:
                self.logger.put("Responding to command: \"{}\": {}".format(data[status_name]['command'], str(data)))

        try:  #send data to client
            for client in CLIENTS:
                client.write_message(data)
        except websocket.WebSocketClosedError:
            pass

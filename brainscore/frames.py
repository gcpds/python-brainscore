import numpy as np
import matplotlib.pyplot as plt


########################################################################
class MplFrame:
    """"""

    #----------------------------------------------------------------------
    def __init__(self, subplot=111):
        """Constructor"""

        self.ax = plt.subplot(subplot, frameon=True)
        self.x = [0]

    ##----------------------------------------------------------------------
    #def __getitem__(self, item):
        #""""""
        #return self.lines[item]

    #----------------------------------------------------------------------
    @property
    def lines(self):
        """"""
        return self._lines

    #----------------------------------------------------------------------
    @lines.setter
    def lines(self, y):
        """"""
        #if self.x is None:
            #if isinstance(y, int):
                #x = np.array(range(y))
            #else:
                #x = np.array(range(y.shape[1]))
        #else:
            #x = self.x

        if isinstance(y, int):
            y = range(y)
            self._lines = [self.ax.plot([0], [0])[0] for line in y]

        else:
            self.ax.set_yticks(range(1, len(y) + 1))
            self.ax.set_yticklabels(y)
            self._lines = [self.ax.plot([0], [0])[0] for line in y]

    #----------------------------------------------------------------------

    @property
    def bars(self):
        """"""
        return self._bars

    #----------------------------------------------------------------------
    @bars.setter
    def bars(self, y):
        """"""
        if self.x is None:
            if isinstance(y, int):
                x = np.array(range(y))
            else:
                x = np.array(range(y.shape[1]))
        else:
            x = self.x

        if isinstance(y, int):
            y = range(y)
            #self._bars = [self.ax.bar(x, [0] * x.shape[0], zorder=3)[0] for line in y]
            self._bars = [self.ax.bar([0], [0], zorder=3)[0] for line in y]

        else:
            self.ax.set_yticks(range(1, len(y) + 1))
            self.ax.set_yticklabels(y)
            #self._bars = [self.ax.bar(x, [0] * x.shape[0], zorder=3)[0] for line in y]
            self._bars = [self.ax.bar([0], [0], zorder=3)[0] for line in y]

# BrainScore

Documentation: https://brainscore.readthedocs.io/en/latest/

** Dependencies **
```bash 

pip install osc4py3 openbci
```

** Stimuli **
```bash 

 python brainscore/stimulus.py
```

** Acquisition **
```bash 

python brainscore/brainscore.py
```